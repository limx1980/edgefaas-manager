FROM alpine:3.11

# Add non root user

RUN apk add --no-cache curl

RUN addgroup -S app && adduser app -S -G app

WORKDIR /home/app/

RUN chown -R app /home/app && mkdir -p /home/app/py && chown -R app /home/app
USER app
ENV PATH=$PATH:/home/app/

USER root

COPY manager.sh .

RUN chown -R app:app ./ && \
  chmod -R 777 /home/app/manager.sh

USER app
